import React, { Component } from 'react';
import { render } from 'react-dom';

import Slider from '../../src';

class Demo extends Component {
  state = {
    threeWay: true,
    value: [0, 0, 0],
  };
  render() {
    return (
      <div>
        <h1>hex-slider Demo</h1>
        <Slider
          isThreeWay={this.state.threeWay}
          onChange={e => this.setState({ value: e })}
          value={this.state.value}
          size={500}
        />
      </div>
    );
  }
}

render(<Demo />, document.querySelector('#demo'));
