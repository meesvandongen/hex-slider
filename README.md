# hex-slider

A React component of a three-way slider. 

## When to use  
If three parts need to be balanced, it can be convenient to use this slider.


## Props

Prop | Type | args | Description
-- | -- | -- | --
knob           | node     |         |Component or element to use for the knob.
size           | number   |         |The width of the element. Excluding 10px margin.
backgroundColor| string   |         |The color of the triangle.
style          | object   |         |The style of the parent of the triangle.
isThreeWay     | bool     |         |A toggle between a two way and three way slider.
onChange       | function*|[A,B,C]  |Fires while the element is being changed.
value          | array    |         |The value of the slider components. [A,B,C] Should add up to one
labelA         | function |A        |The text on the label of angle A
labelB         | function |B        |The text on the label of angle B
labelC         | function |C        |The text on the label of angle C

> Note: "A" is left-bottom, "B" is right-bottom, "C" is (center-)top.

## Example
### Basic (uncontrolled)
```jsx
import Slider from 'tri-slider';

render(
  <Slider
    onChange={e => console.log(e)}
  />, 
  document.querySelector('#root')
);
```

### Controlled
```jsx
import React, { Component } from 'react';
import { render } from 'react-dom';

import Slider from 'tri-slider';

class Demo extends Component {
  state = {
    threeWay: true,
    value: [0.33, 0.33, 0.33],
  }
  render() {
    return (
      <div>
        <h1>tri-slider Demo</h1>
        <button
          onClick={() => { this.setState({ threeWay: !this.state.threeWay }); }}
        >toggle Three-way
          </button>
        <Slider
          isThreeWay={this.state.threeWay}
          onChange={(e) => { this.setState({ value: e }) }}
          value={this.state.value}
        />
      </div>);
  }
}

render(<Demo />, document.querySelector('#demo'));
```

[npm-badge]: https://img.shields.io/npm/v/reuleaux-slider.png?style=flat-square
[npm]: https://www.npmjs.org/package/reuleaux-slider

