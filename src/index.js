import React, { Component } from 'react';
import t from 'prop-types';
import hexagonimg from './img/colored-hexagon-with-triangle.svg';

const cos30 = 0.86602540378;
const heightFromTriangleWidth = size => size / cos30;

const clamp = (val, min, max) => {
  if (val < min) return min;
  if (val > max) return max;
  return val;
};

const minHeightForXPosition = (x, size) => {
  const maxTriangleHeight = size * cos30 - (size / cos30) * 0.5;
  if (x < size / 2) {
    // left top
    return ((size / 2 - x) / (size / 2)) * maxTriangleHeight;
  }
  // right top
  return ((x - size / 2) / (size / 2)) * maxTriangleHeight;
};

const maxHeightForXPosition = (x, size) => {
  const maxTriangleHeight = size * cos30 - (size / cos30) * 0.5;
  if (x < size / 2) {
    // right bottom
    return size / cos30 + ((x - size / 2) / (size / 2)) * maxTriangleHeight;
  }
  // left bottom
  return size / cos30 + ((size / 2 - x) / (size / 2)) * maxTriangleHeight;
};

const clampHexagon = ({ x, y }, size) => {
  x = clamp(x, 0, size);

  if (y > maxHeightForXPosition(x, size)) {
    y = maxHeightForXPosition(x, size);
  } else if (y < minHeightForXPosition(x, size)) {
    y = minHeightForXPosition(x, size);
  }
  return { x, y };
};

export default class ReuleauxSlider extends Component {
  static propTypes = {
    knob: t.node,
    size: t.number,
    backgroundColor: t.string,
    style: t.object,
    isThreeWay: t.bool,
    onChange: t.func.isRequired,
    value: t.array,
    labelA: t.func,
    labelB: t.func,
    labelC: t.func,
  };
  static defaultProps = {
    knob: (
      <div
        style={{
          width: 20,
          height: 20,
          background: '#ccc',
          borderRadius: '50%',
        }}
      />
    ),
    size: 400,
    backgroundColor: '#666',
    style: {},
    isThreeWay: true,
    value: undefined,
    labelA: val => val,
    labelB: val => val,
    labelC: val => val,
  };
  constructor() {
    super();
    this.slider = React.createRef();
  }
  state = {
    coords: { x: 50, y: 50 },
  };

  onMouseDown = (e) => {
    this.onHandleDrag(e);
    document.addEventListener('mouseup', this.onMouseUp);
    document.addEventListener('mousemove', this.onHandleDrag);
    e.preventDefault();
  };
  onMouseUp = (e) => {
    document.removeEventListener('mouseup', this.onMouseUp);
    document.removeEventListener('mousemove', this.onHandleDrag);
    e.preventDefault();
  };
  onHandleDrag = (e) => {
    let coords = this.relativeCoords(e);

    coords = clampHexagon(coords, this.props.size);
    this.setState({ coords });
    e.preventDefault();
  };

  ratiosFromCoords = ({ x, y }) => {
    const height = heightFromTriangleWidth(this.props.size);

    return [a, b, c];
  };

  coordsFromRatios = (ratios) => {
    const height = heightFromTriangleWidth(this.props.size);
    /**
     * tijdelijk
     */
    const x = 50;
    const y = 50;

    return { x, y };
  };

  relativeCoords = (event) => {
    const bounds = this.slider.current.getBoundingClientRect();
    const x = event.clientX - bounds.left;
    const y = event.clientY - bounds.top;
    return { x, y };
  };

  render() {
    const {
      size, style, knob, isThreeWay, value,
    } = this.props;
    // const coords = this.coordsFromRatios(value);
    const { coords } = this.state;
    return (
      <div
        style={{
          width: size,
          margin: 10,
        }}
      >
        <div
          style={{
            textAlign: 'center',
            marginBottom: 10,
            display: isThreeWay ? 'block' : 'none',
          }}
        >
          {this.props.labelC(+value[2].toFixed(2))}
        </div>
        <div
          style={{
            backgroundColor: '#000',
            position: 'relative',
            clipPath: 'polygon(50% 0%, 100% 25%, 100% 75%, 50% 100%, 0% 75%, 0% 25%)',
            overflow: 'hidden',
            width: size,
            height: size / cos30,
            ...style,
          }}
          role="grid"
          tabIndex={0}
          ref={this.slider}
          onMouseDown={this.onMouseDown}
        >
          <div
            style={{
              position: 'absolute',
              background: `url(${hexagonimg})`,
              width: size,
              height: size / cos30,
            }}
          />
          <div
            style={{
              position: 'absolute',
              left: coords.x,
              bottom: size / cos30 - coords.y,
              transform: 'translate(-50%, 50%)',
            }}
          >
            {knob}
          </div>
        </div>
        <div
          style={{
            marginTop: 10,
          }}
        >
          <span>{this.props.labelA(+value[0].toFixed(2))}</span>
          <span style={{ float: 'right' }}>{this.props.labelB(+value[1].toFixed(2))}</span>
        </div>
      </div>
    );
  }
}
